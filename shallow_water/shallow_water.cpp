// shallow_water.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"


#ifndef DELTA_T
//#define DELTA_T 0.014143886
#define DELTA_T 0.0141
#endif

#define PARAM_B 0.3

#define N 100

#ifndef T_END
#define T_END 3
#endif

#define OUTPUT_INTERVAL 0.03
#define EPS 1e-8
// #define SERIES_LOOP 1000


#define auto_const auto const
using std::vector;

template<typename T>
using matrix = vector<vector<T>>;

// 1-dimension shallow water simulation
// ----
// x : position in [0, 1]
// n = a number detemines h
//   n+1 means the sampling number of u: u[n]
//   n   means the sampling number of w: w[n-1]
// h = 1/n : stepping width.
// u(x,t) : horizontal velocity (X-axis positive direction) at position=x, time=t
// w(x,t) : height of wave, where initial height=0 at position=x+h/2, time=t
// 


// initialization
void init(double h, double b, vector<double>& u, vector<double>& w) {
	const auto n = w.size();
	for (int i = 0; i < n; i++) {
		u[i] = 0;
		//if (i < (double)n / 4.0) {
		//	w[i] = 1;
		//}
		//else {
		//	w[i] = 0;
		//}
		//w[i] = 0.2 * sin(7 * M_PI * (i + 1.0 / 2.0) * h) + 0.5;
		//w[i] = cos(4* M_PI * (i+1.0/2.0) * h) * (exp(-(((i + 1.0/2.0)*h)/b) * (((i + 1.0/2.0)*h)/b)))+0.5;
		w[i] = (exp(-(((i + 1.0 / 2.0)*h) / b) * (((i + 1.0 / 2.0)*h) / b)));
	}

	u[u.size()-1] = 0;

	return;
};


// output of the result for each step
void output_step(const int n, const int step, const vector<double>& u, const vector<double> w, const double t, const double h)
{
	char buf[64];
	sprintf_s(buf, "results/waveA_%04d.dat", step);
	std::ofstream ofs(buf);

	// std::cout << "# time = " << t << std::endl;
	ofs << "# time = " << t << std::endl;
	for (int i = 0; i < n; i++) {
		// std::cout << " u[" << i << "]\t= " << u[i] << "\t\tw[" << i << "]\t= " << w[i] << std::endl;
		ofs << (i + 1.0/2.0)*h << "\t" << w[i] << std::endl;
	}
	ofs << std::endl;
	// std::cout << " u[" << N << "]\t= " << u[N] << "\t\tw[" << N << "] = undefined." << std::endl;
	ofs.close();

	return;
}


void time_step(const int n, vector<double>& u, vector<double>& w, const double dt, const double h) {
	
// Runge-Kutta 4, Explicit Scheme

	auto_const rk_level = 4;


	matrix<double> k(rk_level, vector<double>(n+1, 0.0));
	matrix<double> s(rk_level, vector<double>(n, 0.0));
	vector<double> u_tmp(u);
	vector<double> w_tmp(w);

	// represents coefficients of RK4.
	const double b[4] = { 1.0/6.0, 1.0/3.0, 1.0/3.0, 1.0/6.0 };
	const double c[3] = { 0.5, 0.5, 1.0 };

	// Runge Kutta
	for (auto j = 0; j < rk_level; j++) {
		for (auto i = 1; i < n; i++) {
			k[j][i] = (w_tmp[i - 1] - w_tmp[i]) / h;
			s[j][i] = (u_tmp[i] - u_tmp[i + 1]) / h;
		}
		s[j][0] = (u_tmp[0] - u_tmp[1]) / h;
		k[j][0] = 0;
		k[j][n] = 0;

		if (j < rk_level - 1) {
			for (auto i = 0; i < n; i++) {
				u_tmp[i] = u[i] + dt * c[j] * k[j][i];
				w_tmp[i] = w[i] + dt * c[j] * s[j][i];
			}
		}
		else {
			for (auto i = 0; i < n; i++) {
				for (auto l = 0; l < rk_level; l++) {
					u[i] += dt * b[l] * k[l][i];
					w[i] += dt * b[l] * s[l][i];
				}
			}
			//			u[n] = 0;
		}
		
		// u_tmp[n] = 0;
	}

	return;
}

// solver
// dt :
// t_end :
// output_interval : 
// thermal_condition : 
// solver_method : 
void solver(const int n, const double h, const double dt, const double t_end, const double output_interval, vector<double>& u, vector<double>& w) {
	double t, t_output;
	int step=0;	
	
	//char buf[64];
	//sprintf_s(buf,"result/%s_dt%f_N%d_%s.dat",opt_string, DELTA_T, N, mthd_string);
	//std::ofstream ofs("result.dat");

	t_output = 0;
	output_step(n, step++, u, w, 0, h);

	for (t = 0; t < t_end; t += dt) {
		time_step(n, u, w, dt, h);

		t_output += dt;
		if (t_output >= output_interval) {
			t_output -= output_interval;
			output_step(n, step++, u, w, t+dt, h);
		}
	}
};

bool test_RK4() {
	auto_const h = 1.0 / 2.0;
	vector<double> u{0.0,0.0,0.0};
	vector<double> w{1.0, 0.0};
	time_step(2, u, w, 1, h);
	//comparison
	auto err = 0.0;
	err += abs(u[1] - (-2.0 / 3.0));
	err += abs(w[0] - (1.0 / 3.0));
	err += abs(w[1] - (2.0 / 3.0));
	if (err <= EPS)
		return true;
	else
		return false;
}


bool tests() {
// N=4 test?
	int test_num=0;
	auto test_suc=0;

	test_num++;
	if (test_RK4()) test_suc++;

	std::cout << test_suc << " of " << test_num << " tests succeeded." << std::endl;

	return (test_num==test_suc);
}

int main()
{
	auto_const n = N;
	auto_const h = 1.0 / (double) n;
	vector<double> u(n+1, 0.0);
	vector<double> w(n,0.0);

	if (!tests()) {
		return (EXIT_FAILURE);
	}

	init(h, PARAM_B, u, w);
	solver(n, h, DELTA_T, T_END - EPS, OUTPUT_INTERVAL, u, w);
	
    return 0;
}

//todo
//
