# README


## Short Summary

* **Learning C++ (2)**: 1D/2D- Shallow water simulation solver using Runge-Kutta-4.
    - `shallow_water` simulates 1-D shallow water in the simplest way, under Linear Long Wave assumption. 
    - `power_method` is a complement function for detection of eigenvalues of matrix.
    - `shallow_water_dispersion_analysis` is almost the same to `shallow water` and is used for a specific error analysis.
    - `shallow_water_compactFDM` implements Compact Finite Differential Method, some implicit and more accurate one.
    - `nonlinear_dispersive` implements 1D-shallow water simulation using more general and strong model: Peregrine's Equation.
    - `linear_long_2d` implements 2D version of `shallow water`.
* See <http://www-solid.eps.s.u-tokyo.ac.jp/~ataru/edu/index.html> for the original assignment settings.
* Common powerpoint report on this assignment is on the S***Y.

## Execution (for furture)

* This project was created using Visual Studio Enterprise 2015.