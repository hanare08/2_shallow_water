// shallow_water.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"

#define OUTPUT_RESULT true
#define OUTPUT_PERIODIC_ERROR false

#ifndef DELTA_T
//#define DELTA_T 0.014143886
// stable/unstable boundary value for N=100
#define DELTA_T 0.01
#endif

#define PARAM_B 0.3

#define N 100

#ifndef T_END
#define T_END 100
#endif

#define OUTPUT_INTERVAL 1.0
#define ERRORCHECK_INTERVAL 1
#define EPS 1e-8

#define auto_const auto const

// 1-dimension shallow water simulation with Peregrine's Equation
// ----
// x : position in [0, 1]
// n = a number detemines h
// h = 1/n : stepping width.
// w(x,t) : height of wave, where initial height=0 at position=x+h/2, time=t
// 

using std::vector;

template <typename T>
using matrix = vector<vector<T>>;

// returns initial wave height w(x, t=0)
double initial_height(double x) {
	auto_const b = PARAM_B;
	// square wave
	//if (x < 1.0/4.0)
	//	return 1;
	//else
	//	return 0;

// normal distributional wave
	//return (exp(-(x / b) * (x / b)));

// cosine wave
	return(1.5 * cos(2 * M_PI * (1 - x)) * exp(-((1 - x) / b)));
}


// returns depth compared to static water,
// positive values are required for all x (0 < x < 1)
double depth(double x) {
	//	return 1.0;
	return(5.0 - cos(1.5 * M_PI * (1 - x) * exp(-(1 - x)* (1 - x))));
}


// initialization of u, w, depth, F?
void init(int n, double h, vector<double>& w, vector<double>& u, vector<double>& d) {
	for (int i = 0; i < n; i++) {
		w[i] = initial_height((i + 1.0 / 2.0) * h);
		u[i] = 0;
		d[i] = depth(i*h);
	}
	u[n] = 0;
	d[n] = depth(1.0);

	return;
};


// output of the result for each step
void output_step(const int n, int step, vector<double>& w, double t, double h)
{
	char buf[64];
	sprintf_s(buf, "results/waveD_%04d.dat", step);
	std::ofstream ofs(buf);

	std::cout << "# time = " << t << std::endl;
	ofs << "# time = " << t << std::endl;
	for (int i = 0; i < n; i++) {
		std::cout << " w[" << i << "]\t= " << w[i] << std::endl;
		ofs << (i + 0.5) * h << "\t" << w[i] << std::endl;
	}
	ofs << std::endl;
	ofs.close();

	return;
}


// Outputs depth data d[0] .. d[n-1] to .dat file.
void output_bottomdata(const int n, const double h, const vector<double>& depth)
{
	std::ofstream ofs("results/bottom.dat");

	for (int i = 0; i < n; i++) {
		ofs << i * h << "\t" << -depth[i] << std::endl;
	}
	ofs << std::endl;
	ofs.close();

	return;
}


// Initalizes f[0] .. f[n-1] by fill in 0.
// Strictly, F should be initialized using u, when u is given in general form.
// This version of init_f assumes u[i]= 0 for all i, when time t=0.
void init_f(const int n, vector <double>& f) {
	for (auto i = 0; i < n; i++) {
		f[i] = 0.0;
	}
};


// *---*---*---*---
// Fills in 3 vectors coef[0], coef[1], coef[2]: each of which respectively represents A[i-1][i], A[i][i], A[i][i+1],
// where A[-1][0] = A[n][n+1] = 0 for descriptive purposes.
void init_coefmat(matrix<double>& coef_mat, vector<double> d, const double h) {
	const auto n = coef_mat[0].size();
	coef_mat[0][0] = 0;
	coef_mat[1][0] = 1.0 + (2.0 * d[1] * d[1]) / (3.0 * h * h);
	coef_mat[2][0] = -d[1] * (3.0 * d[2] - d[1]) / (6.0 * h * h);

	for (auto i = 1; i < n - 1; i++) {
		coef_mat[0][i] = -d[i + 1] * (3.0 * d[i] - d[i + 1]) / (6.0 * h * h);
		coef_mat[1][i] = 1.0 + (2.0 * d[i + 1] * d[i + 1]) / (3.0 * h * h);
		coef_mat[2][i] = -d[i + 1] * (3.0 * d[i + 2] - d[i + 1]) / (6.0 * h * h);
	}

	if (n > 2) {
		coef_mat[0][n - 1] = -d[n - 1] * (3.0 * d[n - 2] - d[n - 1]) / (6.0 * h * h);
		coef_mat[1][n - 1] = 1.0 + (2.0 * d[n - 1] * d[n - 1]) / (3.0 * h * h);
	}
	coef_mat[2][n - 1] = 0;
	return;

};

// convert tridiagonal matrix into convenient form.
void init_tridiag_solver(matrix<double>& coef_mat) {
	auto_const n = coef_mat[0].size();
	for (int i = 0; i < n; i++) {
		if (i == 0) {
			coef_mat[1][0] = 1.0 / coef_mat[1][0];
		}
		else {
			coef_mat[1][i] = 1.0 / (coef_mat[1][i] - coef_mat[0][i] * coef_mat[2][i - 1]);
			coef_mat[0][i] = coef_mat[1][i] * coef_mat[0][i];
		}
		coef_mat[2][i] = coef_mat[1][i] * coef_mat[2][i];
	}
	return;
}
;


// *---*---*---*---*---*---*---*---*---*---*---
// !! n x n matrix version !!
// Linear Equation Solver, using Gauss Elimination.
// Specific for tri-diagonal coefficient matrrix.
// Given Ax=r as 
// coef_mat[0]:A[i-1][i]
// b:A[i][i]
// c:A[i+1][i]
void tridiag_solver(const matrix<double>& coef_mat, vector<double>& r) {
	auto_const n = coef_mat[0].size();
	// forward elimination
	r[0] = coef_mat[1][0] * r[0];
	for (int i = 1; i < n; i++) {
		r[i] = coef_mat[1][i] * r[i] - coef_mat[0][i] * r[i - 1];
	}
	// backward substitution
	for (int i = n - 2; 0 <= i; i--) {
		r[i] = r[i] - coef_mat[2][i] * r[i + 1];
	}
	return;
}
;

void calc_k(vector<double>& kj, const vector<double>& w, const vector<double>& u, const vector<double>& d, const double h) {
	auto_const n = kj.size();
	kj[0] = -(2.0 * d[1] + w[0] + w[1]) * u[1] / (2.0 * h);
	for (auto i = 1; i < n - 1; i++) {
		kj[i] = ((2.0 * d[i] + w[i - 1] + w[i]) * u[i] - (2.0 * d[i + 1] + w[i] + w[i + 1]) * u[i + 1]) / (2.0 * h);
	}
	kj[n - 1] = (2.0 * d[n - 1] + w[n - 2] + w[n - 1]) * u[n - 1] / (2.0 * h);
	return;
}

void calc_s(vector<double>& sj, const vector<double>& w, const vector<double>& u, const double h) {
	auto_const n = w.size();
	for (auto i = 0; i < n - 1; i++) {
		sj[i] = u[i + 1] * (u[i] - u[i + 2]) / (2.0 * h) + (w[i] - w[i + 1]) / h;
	}
}

void calc_u(vector<double>& u, const vector<double>& f, const matrix<double>& coef_mat) {
	auto_const n = f.size();
	// call tridiag solver.
	// should be modified later... using u+1 , f+1? 
	vector<double> u_tmp(f);
	tridiag_solver(coef_mat, u_tmp);
	for (auto i = 0; i < n; i++) {
		u[i + 1] = u_tmp[i];
	}
	// u[0] = 0.0;
// u[n] = 0.0;
	return;
};



// Simluates time-integration with Runge-Kutta 4, Explicit Scheme.
void time_step(vector<double>& w, vector<double> &f, vector<double>& u, double dt, double h, const vector<double>& d, const matrix<double>& coef_mat) {

	auto_const n = w.size();
	auto_const rk_level = 4;
	const double b[rk_level] = { 1.0 / 6.0, 1.0 / 3.0, 1.0 / 3.0, 1.0 / 6.0 };
	const double c[rk_level - 1] = { 0.5, 0.5, 1.0 };

	vector<double> 	w_tmp(w), f_tmp(f);
	matrix<double> k(rk_level, vector<double>(n, 0.0)), s(rk_level, vector<double>(n - 1, 0.0));

	w_tmp = w;
	f_tmp = f;

	// Runge Kutta step 1~rk_level
	for (auto j = 0; j < rk_level; j++) {

		calc_k(k[j], w_tmp, u, d, h);
		calc_s(s[j], w_tmp, u, h);

		if (j < rk_level - 1) {
			for (auto i = 0; i < n - 1; i++) {
				w_tmp[i] = w[i] + dt * c[j] * k[j][i];
				f_tmp[i] = f[i] + dt * c[j] * s[j][i];
			}
			w_tmp[n - 1] = w[n - 1] + dt * c[j] * k[j][n - 1];
			calc_u(u, f_tmp, coef_mat);
		}
		else {
			for (auto l = 0; l < rk_level; l++) {
				for (auto i = 0; i < n - 1; i++) {
					w[i] += dt * b[l] * k[l][i];
					f[i] += dt * b[l] * s[l][i];
				}
				w[n - 1] += dt * b[l] * k[l][n - 1];
				// f[0] and f[n] will not be used.
			}
			calc_u(u, f, coef_mat);
		}

	}

	return;
}




// solver
// dt :
// t_end :
// output_interval : 
// thermal_condition : 
// solver_method : 
void solver(double h, double dt, double t_end, double output_interval, vector<double>& w, vector<double>& u, vector<double>& d) {
	auto_const n = w.size();
	double t, t_output, t_errcheck;
	int step = 0, step_e = 0;
	int progress = 0;

	vector<double> f(n - 1);
	init_f(n - 1, f);


	//	std::ofstream ofs_w0("results/w0.dat");

	matrix<double> coef_mat(3, vector<double>(n - 1));
	init_coefmat(coef_mat, d, h);
	init_tridiag_solver(coef_mat);

	t_output = 0;
	t_errcheck = 0;
#if OUTPUT_RESULT
	output_step(n, step++, w, 0, h);
#endif


	for (t = 0; t < t_end; t += dt) {
		time_step(w, f, u, dt, h, d, coef_mat);

		t_output += dt;
		t_errcheck += dt;

		if (t_output >= output_interval) {
			progress = (int)(t * 100 / t_end);
			std::cerr << "processing (N=" << n << ") ... " << progress << "%\r" << std::flush;
			t_output -= output_interval;
			//			ofs_w0 << t << "\t" << z[0] << std::endl;
#if OUTPUT_RESULT
			output_step(n, step++, w, t + dt, h);
#endif
		}

	}
	std::cerr << "processing (N=" << n << ") ... " << "100% done." << std::endl;

};


bool test_tridiag_solver() {
	const int n = 3;
	matrix<double> coef_mat{ {0,0,0},{1,1,1},{1,1,0} };
	vector<double> r{ 2,2,1 };
	init_tridiag_solver(coef_mat);
	tridiag_solver(coef_mat, r);

	double err = 0.0;
	for (auto i = 0; i < n; i++) {
		err += abs(r[i] - 1.0);
	}
	return(err < EPS);

}



bool tests() {
	// N=4 test?
	int test_num = 0;
	auto test_suc = 0;

	// tridiag solver
	test_num++;
	if (test_tridiag_solver()) test_suc++;



	std::cerr << test_suc << " of " << test_num << " tests succeeded." << std::endl;

	return (test_num == test_suc);
}

void case_n(int n) {
	auto_const h = 1.0 / (double)n;
	vector<double> w(n), u(n + 1), d(n + 1);
	double tmp_t;

	if (n < 2) {
		std::cerr << "N must be larger than 1." << std::endl;
		exit(EXIT_FAILURE);
	}

	if (n < 100)
		tmp_t = DELTA_T;
	else
		tmp_t = 0.001;

	init(n, h, w, u, d);

	output_bottomdata(n + 1, h, d);

	solver(h, tmp_t, T_END + EPS, OUTPUT_INTERVAL, w, u, d);
}

int main()
{

	if (!tests()) {
		return (EXIT_FAILURE);
	}

	//case_n<20>();
	//case_n<40>();
	//case_n<60>();
	//case_n<80>();
	//case_n<100>();
	//case_n<200>();
	//case_n<300>();
	//case_n<400>();
	case_n(N);

	return 0;
}
