// shallow_water.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"

#define OUTPUT_RESULT true
#define OUTPUT_PERIODIC_ERROR true

#ifndef DELTA_T
//#define DELTA_T 0.014143886
// stable/unstable boundary value for N=100
#define DELTA_T 0.01
#endif

#define PARAM_B 0.3

#define N 100

#ifndef T_END
#define T_END 100000
#endif

#define OUTPUT_INTERVAL 1000
#define ERRORCHECK_INTERVAL 100
#define EPS 1e-8

#define auto_const auto const

// 1-dimension shallow water simulation with compact FDM
// ----
// x : position in [0, 1]
// n = a number detemines h
// h = 1/n : stepping width.
// w(x,t) : height of wave, where initial height=0 at position=x+h/2, time=t
// 

template<typename T> void copy_array(const int n, T dst[], T src[]) {
	for (auto i = 0; i < n; i++) {
		dst[i] = src[i];
	}
}

double initial_height(double x, double b) {

// square wave
	//if (x < 1.0/3.0)
	//	return 1;
	//else
	//	return 0;

// normal distributional wave
	return (exp(-(x / b) * (x / b)));

// cosine wave
//	return(1.5 * cos(9 * M_PI * x) * exp(-(x / b)));
}

// initialization
void init(int n,  double h, double b, double w[], double z[]) {
	for (int i = 0; i < n; i++) {
		w[i] = initial_height(i * h, b);
		z[i] = 0;
	}

	return;
};


// output of the result for each step
void output_step(const int n, int step, double w[], double z[], double t, double h)
{
	char buf[64];
	sprintf_s(buf, "results/waveC_%04d.dat", step);
	std::ofstream ofs(buf);

//	std::cout << "# time = " << t << std::endl;
	ofs << "# time = " << t << std::endl;
	for (int i = 0; i < n; i++) {
//		std::cout << " w[" << i << "]\t= " << w[i] << std::endl;
		ofs << i * h << "\t" << w[i] << std::endl;
	}
	ofs << std::endl;
	ofs.close();

	return;
}

// output of the result for each step
void output_periodic_error(const int n, int step, double w[], double w0[], double t, double h, std::ofstream &efs)
{

	double err = 0.0;
	for (int i = 0; i < n; i++) {
		err += (w[i] - w0[i]) * (w[i] - w0[i]);
	}
	err = sqrt(err / n);

	efs << t << "\t" << err << std::endl;

	return;
}



// Fills in 3 vectors a, b, c: each of which respectively represents A[i-1][i], A[i][i], A[i][i+1],
// where A[-1][0] = A[n-1][n] = 0 for descriptive purposes.
// For this time, initialization coefficiency matrix is for secondary differential coefficient f" in compact FDM.
void tridiag_init(const int n, double a[], double b[], double c[]) {
	a[0] = 0;
	b[0] = 2.0 / 3.0;
	c[0] = 1.0 / 3.0;
	for (auto i = 1; i < n - 1; i++) {
		a[i] = 1.0 / 12.0;
		b[i] = 5.0 / 6.0;
		c[i] = 1.0 / 12.0;
	}
	a[n-1] = 1.0 / 3.0;
	b[n-1] = 2.0 / 3.0;
	c[n-1] = 0;
	return;

};

// convert tridiagonal matrix into useful form.
void tridiag_solver_init(int n, double a[], double b[], double c[]) {
	for (int i = 0; i < n; i++) {
		if (i == 0) {
			b[0] = 1.0 / b[0];
		}
		else {
			b[i] = 1.0 / (b[i] - a[i] * c[i - 1]);
			a[i] = b[i] * a[i];
		}
		c[i] = b[i] * c[i];
	}
	return;
}
;

// Linear Equation Solver, using Gauss Elimination.
// Specific for tri-diagonal coefficient matrrix.
// Given Ax=r as 
// a:A[i-1][i]
// b:A[i][i]
// c:A[i+1][i]
void tridiag_solver(int n, double a[], double b[], double c[], double r[]) {
	// forward elimination
	r[0] = b[0] * r[0];
	for (int i = 1; i < n; i++) {
		r[i] = b[i] * r[i] - a[i] * r[i - 1];
	}
	// backward substitution
	for (int i = n-2; 0 <= i; i--) {
		r[i] = r[i] - c[i] * r[i + 1];
	}
	return;
}
;



template<const int n> void calc_k(double h, double w[], double kj[], double coef_mat[][n]) {
	kj[0] = (2 * w[1] - 2 * w[0]) / (h * h);
	for (auto i = 1; i < n-1; i++) {
		kj[i] = (w[i - 1] + w[i + 1] - 2 * w[i]) / (h * h);
	}
	kj[n - 1] = (2 * w[n - 2] - 2 * w[n - 1]) / (h * h);

	tridiag_solver(n, coef_mat[0], coef_mat[1], coef_mat[2], kj);
}

template<const int n> void calc_s(double z[], double sj[]) {
	for (auto i = 0; i < n; i++) {
		sj[i] = z[i];
	}
}

template<const int n> void time_step(double w[], double z[], double dt, double h) {
	
// Runge-Kutta 4, Explicit Scheme
	auto_const rk_level = 4;
	const double b[rk_level] = { 1.0/6.0, 1.0/3.0, 1.0/3.0, 1.0/6.0 };
	const double c[rk_level-1] = { 0.5, 0.5, 1.0 };
	double 	w_tmp[n], z_tmp[n], k[rk_level][n], s[rk_level][n], coef_mat[3][n];

	copy_array<double>(n, w_tmp, w);
	copy_array<double>(n, z_tmp, z);

	// need to modify later...
	tridiag_init(n, coef_mat[0], coef_mat[1], coef_mat[2]);
	tridiag_solver_init(n, coef_mat[0], coef_mat[1], coef_mat[2]);

	// Runge Kutta step 1~rk_level
	for (auto j = 0; j < rk_level; j++) {

		calc_k<n>(h, w_tmp, k[j], coef_mat);
		calc_s<n>(z_tmp, s[j]);

		if (j < rk_level - 1) {
			for (auto i = 0; i < n; i++) {
				z_tmp[i] = z[i] + dt * c[j] * k[j][i];
				w_tmp[i] = w[i] + dt * c[j] * s[j][i];
			}
		}
		else {
			for (auto i = 0; i < n; i++) {
				for (auto l = 0; l < rk_level; l++) {
					z[i] += dt * b[l] * k[l][i];
					w[i] += dt * b[l] * s[l][i];
				}
			}
		}

	}

	return;
}




// solver
// dt :
// t_end :
// output_interval : 
// thermal_condition : 
// solver_method : 
template<int n> void solver(double h, double dt, double t_end, double output_interval, double w[], double z[]) {
	double t, t_output,t_errcheck;
	int step=0, step_e=0;	
	int progress = 0;

	double w0[n];
	copy_array<double>(n, w0, w);

#if OUTPUT_PERIODIC_ERROR
	char buf[128];
	sprintf_s(buf, "error/dispersion_N%04d.dat", n);
	std::ofstream efs(buf);
#endif
//	std::ofstream ofs_w0("results/w0.dat");

	//tridiag_init(a, b, c, h, dt);
	//tridiag_solver_init(a, b, c);

	t_output = 0;
	t_errcheck = 0;
#if OUTPUT_RESULT
	output_step(n, step++, w, z, 0, h);
#endif
#if OUTPUT_PERIODIC_ERROR
	output_periodic_error(n, step_e++, w, w0, 0, h, efs);
#endif

	for (t = 0; t < t_end; t += dt) {
		time_step<n>(w, z, dt, h);

		t_output += dt;
		t_errcheck += dt;

		if (t_output >= output_interval) {
			progress = (int)(t * 100 / t_end);
			std::cerr << "processing (N=" << n << ") ... " << progress << "%\r" << std::flush;
			t_output -= output_interval;
//			ofs_w0 << t << "\t" << z[0] << std::endl;
#if OUTPUT_RESULT
			output_step(n, step++, w, z, t+dt, h);
#endif
		}

#if OUTPUT_PERIODIC_ERROR
		if (t_errcheck >= ERRORCHECK_INTERVAL) {
			t_errcheck -= ERRORCHECK_INTERVAL;
			output_periodic_error(n, step_e++, w, w0, t + dt, h, efs);
		}
#endif
	}
	std::cerr << "processing (N=" << n << ") ... " << "100% done." << std::endl;

};



bool test_RK4() {
	return true;
	//auto_const h = 1.0 / 2.0;
	//double w[3] = {1.0,0.0,0.0};
	//double z[3] = {0.0,0.0,0.0};
	//time_step(3, w, z, 1, h);
	////comparison
	//auto err = 0.0;
	//err += abs(w[1] - (-2.0 / 3.0));
	//err += abs(w[0] - (1.0 / 3.0));
	//err += abs(w[1] - (2.0 / 3.0));
	//if (err <= EPS)
	//	return true;
	//else
	//	return false;
}


bool tests() {
	// N=4 test?
	int test_num=0;
	auto test_suc=0;

	// 1st test: RK4 - small case.
	test_num++;
	if (test_RK4()) test_suc++;

	std::cerr << test_suc << " of " << test_num << " tests succeeded." << std::endl;

	return (test_num==test_suc);
}

template<int n> void case_n() {
	auto_const h = 1.0 / (double) (n-1);
	double w[n];
	double z[n];
	double tmp_t;

	if (n < 3) {
		std::cerr << "N must be larger than 2." << std::endl;
		exit(EXIT_FAILURE);
	}

	if (n < 100)
		tmp_t = DELTA_T;
	else
		tmp_t = 0.001;

	init(n, h, PARAM_B, w, z);
	solver<n>(h, tmp_t, T_END + EPS, OUTPUT_INTERVAL, w, z);
}

int main()
{

	if (!tests()) {
		return (EXIT_FAILURE);
	}

	case_n<20>();
	case_n<40>();
	//case_n<60>();
	case_n<80>();
	case_n<100>();
	//case_n<200>();
	//case_n<300>();
	//case_n<400>();
	//case_n<N>();

    return 0;
}

//todo
// support coef_mat[0], [1], [2] where a,b,c is used now.
// coef_mat should be prepared earlier.(should not be prepared in time_step().)
