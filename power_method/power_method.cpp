// power_method.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"

#define EPS (1.0e-8)
#define EPS_TEST (1.0e-6)
#define N 101

using std::vector;

template<typename T>
using matrix = vector<vector<T>>;

inline double inner_product(const vector<double>& v, const vector<double>& w) {
	double sum = 0.0;
	const int n = v.size();
	for (auto i = 0; i < n; i++) {
		sum += v[i] * w[i];
	}
	return (sum);
}


inline double norm_2(const vector<double>& v) {

	return (sqrt(inner_product(v, v)));
}

inline void normalize_vector(vector<double>& v) {
	const int n = v.size();
	double norm = norm_2(v);
	for (auto i = 0; i < n; i++) {
		v[i] = v[i] / norm;
	}
	return;
}

void mul_mat_vec(const matrix<double>& a, const vector<double>& v, vector<double>& c) {
	const int n = a.size();
	for (auto i = 0; i < n; i++) {
		c[i] = 0;
		for (auto j = 0; j < n; j++) {
			c[i] += a[i][j] * v[j];
		}
	}
	return;
}


void  power_method(double &ev, vector<double>& v, const matrix<double>& a) {
	const int n = a.size();
	double z;
	vector<double> v_new(v);

	while (true) {
		normalize_vector(v);
		mul_mat_vec(a, v, v_new);
		z = inner_product(v, v_new);
		
		if (abs(ev - z) < EPS) {
			break;
		}
		else {
//			std::cout << abs(ev - z) << std::endl;
			ev = z;
			v.assign(v_new.begin(), v_new.end());
		}
	}
}


bool test_pm() {
	const int n = 3;
	vector<double> v{ 1.0,0.0,0.0 };
	matrix<double> a{ {-1,1,0}, {1,-2,1}, {0,1,-1} };
	double exact_v[3] = { -1.0 / sqrt(6.0), 2.0 / sqrt(6.0), -1.0 / sqrt(6.0) };
	double ev = 0.0;
	double exact = -3.0;
	double err = 0.0;
	power_method(ev, v, a);
	err += abs(exact - ev);

	return (err < EPS_TEST);
}


void init_matrix(matrix<double>& a) {
	//double h = 1.0 / (double)N;
	const int n = a.size();


	a[0][0] = -1.0;
	a[0][1] = 1.0;
	for (auto j = 2; j < n; j++) {
		a[0][j] = 0;
	}

	for (auto i = 1; i < n-1; i++) {
		for (auto j = 0; j < i-1; j++) {
			a[i][j] = 0;
		}
		a[i][i - 1] = 1.0;
		a[i][i] = -2.0;
		a[i][i + 1] = 1.0;
		for (auto j = i+2; j < n; j++) {
			a[i][j] = 0;
		}
	}

	for (auto j = 0; j < n-2; j++) {
		a[n-1][j] = 0;
	}
	a[n-1][n-2] = 1.0;
	a[n-1][n-1] = -1.0;


}

void init_vector(vector<double>& v) {
	const int n = v.size();
	v[0] = 0.0;
	for (auto i = 1; i < n; i++) {
		v[i] = 1.0 / sqrt(n);
	}

}



int main()
{

	assert(test_pm());

	const int n = N;

	matrix<double> a(n, vector<double>(n, 0.0));
	vector<double> v(n, 0.0);

	init_matrix(a);
	init_vector(v);

	double ev = 0.0;

	power_method(ev,v,a);

	std::cout << "maximum eigenvalue : " << ev << std::endl;

    return 0;
}

/*
To do
c

input:
A : given matrix
n : size of A

output:

v : eigenvector
lambda : eigenvalue
*/