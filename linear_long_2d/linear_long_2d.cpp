// shallow_water.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"


#ifndef DELTA_T
//#define DELTA_T 0.014143886
#define DELTA_T 0.001
#endif

#define PARAM_B 0.3

#define N 100

#ifndef T_END
#define T_END 5
#endif

#define OUTPUT_INTERVAL 0.05
#define EPS 1e-8
// #define SERIES_LOOP 1000


#define auto_const auto const
using std::vector;
using std::for_each;

template<typename T>
using matrix = vector<vector<T>>;

// 1-dimension shallow water simulation
// ----
// x : position in [0, 1]
// n = a number detemines h
//   n+1 means the sampling number of u: u[n]
//   n   means the sampling number of w: w[n-1]
// h = 1/n : stepping width.
// u(x,t) : horizontal velocity (X-axis positive direction) at position=x, time=t
// w(x,t) : height of wave, where initial height=0 at position=x+h/2, time=t
// 

// returns initial height w(x,y)
inline double initial_height(double x, double y) {
	return (3 * exp(-10 * ((x - 0.3)*(x - 0.3) + (y - 0.3)*(y - 0.3))));
}

// initialization
// z=cos(3*x)cos(2*y)exp(-x^2) ?
void init(const int nx, const int ny, matrix<double>& u, matrix<double>& v, matrix<double>& w) {
	for_each(u.begin(), u.end(), [](vector<double>& row) {row.assign(row.size(), 0.0); });
	for_each(v.begin(), v.end(), [](vector<double>& row) {row.assign(row.size(), 0.0); });

	for (int i = 0; i < ny; i++) {
		for (int j = 0; j < nx; j++) {
			w[i][j] = initial_height((j + 0.5) / (double)nx, (i + 0.5) / (double)ny);
		}
	}

	return;
};


// output of the result for each step
void output_step(const int nx, const int ny, const int step, const matrix<double>& w, const double t, const double dx, const double dy)
{
	char buf[64];
	sprintf_s(buf, "results/wave2D_A_%04d.dat", step);
	std::ofstream ofs(buf);

	// std::cout << "# time = " << t << std::endl;
	ofs << "# time = " << t << std::endl;
	for (int j = 0; j < nx; j++) {
		for (int i = 0; i < ny; i++) {

			ofs << (j + 1.0 / 2.0)*dx << "\t"
				<< (i + 1.0 / 2.0)*dy << "\t"
				<< w[i][j] << std::endl;
			//std::cout << (j + 1.0 / 2.0)*dx << "\t"
			//	<< (i + 1.0 / 2.0)*dy << "\t"
			//	<< w[i][j] << std::endl;
		}
	}
	ofs << std::endl;
	ofs.close();

	return;
}


void time_step(const int nx, const int ny, matrix<double>& u, matrix<double>& v, matrix<double>& w, const double dt, const double dx, const double dy) {

	// Runge-Kutta 4, Explicit Scheme

	auto_const rk_level = 4;

	vector<matrix<double>> k(rk_level, u);
	vector<matrix<double>> s(rk_level, v);
	vector<matrix<double>> l(rk_level, w);
	matrix<double> u_tmp(u);
	matrix<double> v_tmp(v);
	matrix<double> w_tmp(w);

	// represents coefficients of RK4.
	const double b[4] = { 1.0 / 6.0, 1.0 / 3.0, 1.0 / 3.0, 1.0 / 6.0 };
	const double c[3] = { 0.5, 0.5, 1.0 };

	// Runge Kutta loop
	for (auto rk_step = 0; rk_step < rk_level; rk_step++) {

		// boundary 1: nearby y==0

		// s[rk_step][0][j] = 0;
		// k[rk_step][0][0] = 0;
		l[rk_step][0][0] = (u_tmp[0][0] - u_tmp[0][1]) / dx + (v_tmp[0][0] - v_tmp[1][0]) / dy;
		for (auto j = 1; j < nx; j++) {
			k[rk_step][0][j] = (w_tmp[0][j - 1] - w_tmp[0][j]) / dx;
			l[rk_step][0][j] = (u_tmp[0][j] - u_tmp[0][j + 1]) / dx + (v_tmp[0][j] - v_tmp[1][j]) / dy;
		}
		// k[rk_step][0][nx] = 0;


		// intermediate: nearby 0<y<1	

		for (auto i = 1; i < ny; i++) {
			// boundary 2: x==0
			// k[rk_step][i][0] = 0;
			s[rk_step][i][0] = (w_tmp[i - 1][0] - w_tmp[i][0]) / dy;
			l[rk_step][i][0] = (u_tmp[i][0] - u_tmp[i][1]) / dx + (v_tmp[i][0] - v_tmp[i + 1][0]) / dy;


			// intermediate: 0<x<1

			for (auto j = 1; j < nx; j++) {
				k[rk_step][i][j] = (w_tmp[i][j - 1] - w_tmp[i][j]) / dx;
				s[rk_step][i][j] = (w_tmp[i - 1][j] - w_tmp[i][j]) / dy;
				l[rk_step][i][j] = (u_tmp[i][j] - u_tmp[i][j + 1]) / dx + (v_tmp[i][j] - v_tmp[i + 1][j]) / dy;
			}

			// boundary 3: x==1 (j==nx, v,w undefined)
			// k[rk_step][i][nx] = 0;

		}

		// boundary 4: y==1 (i==ny, u,w undefined)
		// for (auto j = 0; j < nx; j++){
		//     s[rk_step][ny][j] = 0;
		// }


	// prepare for next rk4 loop		
		if (rk_step < rk_level - 1) {
			for (auto i = 0; i < ny; i++) {
				for (auto j = 0; j < nx; j++) {
					u_tmp[i][j] = u[i][j] + dt * c[rk_step] * k[rk_step][i][j];
					v_tmp[i][j] = v[i][j] + dt * c[rk_step] * s[rk_step][i][j];
					w_tmp[i][j] = w[i][j] + dt * c[rk_step] * l[rk_step][i][j];
				}
			}
		}
	}

	// reflection to original variables: u,v,w	
	for (auto i = 0; i < ny; i++) {
		for (auto j = 0; j < nx; j++) {
			for (auto rk = 0; rk < rk_level; rk++) {
				u[i][j] += dt * b[rk] * k[rk][i][j];
				v[i][j] += dt * b[rk] * s[rk][i][j];
				w[i][j] += dt * b[rk] * l[rk][i][j];
			}
		}
	}

	// if u,v != 0 on boundaries, this type of update equations must be added here.


	return;
}

// solver
// dt :
// t_end :
// output_interval : 
// thermal_condition : 
// solver_method : 
void solver(const int nx, const int ny, const double dx, const double dy, const double dt, const double t_end, const double output_interval, matrix<double>& u, matrix<double>& v, matrix<double>& w) {
	double t, t_output;
	int step = 0;

	//char buf[64];
	//sprintf_s(buf,"result/%s_dt%f_N%d_%s.dat",opt_string, DELTA_T, N, mthd_string);
	//std::ofstream ofs("result.dat");

	t_output = 0;
	output_step(nx, ny, step++, w, 0, dx, dy);

	for (t = 0; t < t_end; t += dt) {
		time_step(nx, ny, u, v, w, dt, dx, dy);

		t_output += dt;
		if (t_output >= output_interval) {
			t_output -= output_interval;
			output_step(nx, ny, step++, w, t + dt, dx, dy);
		}
	}
};

bool test_RK4() {
	auto_const nx = 2, ny = 1;
	auto_const dx = 1.0 / (double)nx, dy = 1.0 / (double)ny;
	matrix<double> u{ {0.0,0.0,0.0} };
	matrix<double> v{ {0.0,0.0}, {0.0,0.0} };
	matrix<double> w{ {1.0, 0.0} };
	time_step(2, 1, u, v, w, 1, dx, dy);
	//comparison
	auto err = 0.0;
	//err += abs(u[1] - (-2.0 / 3.0));
	//err += abs(w[0] - (1.0 / 3.0));
	//err += abs(w[1] - (2.0 / 3.0));
	if (err <= EPS)
		return true;
	else
		return false;
}


bool tests() {
	// N=4 test?
	int test_num = 0;
	auto test_suc = 0;

	test_num++;

	//if (test_RK4())
	test_suc++;


	std::cout << test_suc << " of " << test_num << " tests succeeded." << std::endl;

	return (test_num == test_suc);
}

int main()
{
	auto_const nx = N, ny = N;
	auto_const dx = 1.0 / (double)nx, dy = 1.0 / (double)ny;
	matrix<double> u(ny, vector<double>(nx + 1, 1.0));
	matrix<double> v(ny + 1, vector<double>(nx, 1.0));
	matrix<double> w(ny, vector<double>(nx));

	if (!tests()) {
		return (EXIT_FAILURE);
	}

	init(nx, ny, u, v, w);

	solver(nx, ny, dx, dy, DELTA_T, T_END - EPS, OUTPUT_INTERVAL, u, v, w);

	return 0;
}

//todo
//
